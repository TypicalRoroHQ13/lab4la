package linearalgebra;

import static org.junit.Assert.*;
import org.junit.Test;
/*
 * Safin Haque
 * 2132492
 */

public class Vector3dTest {
    
    @Test
    public void getMethod_returnsCorrectValues(){
        Vector3d test= new Vector3d(5,3,2);

        assertEquals(5, test.getX(), 0.1);
        assertEquals(3, test.getY(), 0.1);
        assertEquals(2, test.getZ(), 0.1);
    }

    @Test
    public void magnitude_return9(){
        Vector3d test2=new Vector3d(1, 2, 2);

        assertEquals(3, test2.magnitude(), 0.1);
    }

    @Test
    public void dotProduct_return14(){
        Vector3d vector1=new Vector3d(1,2,3);
        Vector3d vector2=new Vector3d(1,2,3);

        assertEquals(14, vector1.dotProduct(vector2), 0.1);
    }

    @Test
    public void add_return246(){
        Vector3d vector1=new Vector3d(1,2,3);
        Vector3d vector2=new Vector3d(1,2,3);
        Vector3d addedVector=vector1.add(vector2);

        assertEquals(2, addedVector.getX(), 0.1);
        assertEquals(4, addedVector.getY(), 0.1);
        assertEquals(6, addedVector.getZ(), 0.1);

        
    }
}
