package linearalgebra;
/*
 * Safin Haque
 * 2132492
 */
public class Vector3d{
    public double x;
    public double y;
    public double z;


    public Vector3d(double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((Math.pow(x,2)+Math.pow(y,2)+Math.pow(z,2)));
    }

    public double dotProduct(Vector3d secondObject){
        return ((this.x*secondObject.getX())+(this.y*secondObject.getY())+(this.z*secondObject.getZ()));
    }

    public Vector3d add(Vector3d otherObject){
        return new Vector3d(this.x+otherObject.getX(), this.y+otherObject.getY(), this.z+otherObject.getZ());
    }
}